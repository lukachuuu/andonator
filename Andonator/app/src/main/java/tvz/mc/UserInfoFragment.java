package tvz.mc;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import tvz.mc.Helper.SessionManager;


public class UserInfoFragment extends Fragment {

    private SessionManager sm;
    private ImageView imageView;
    private TextView ime_prezime, email, donacija;
    private Button logootBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sm = new SessionManager(getActivity());


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //postavljanje view-a
        View view = inflater.inflate(R.layout.user_info, container, false);

        //povezivanje s id-om
        imageView = (ImageView) view.findViewById(R.id.imageView_info);
        ime_prezime = (TextView) view.findViewById(R.id.username_info);
        email = (TextView) view.findViewById(R.id.email_info);
        donacija = (TextView) view.findViewById(R.id.donacije_info);

        logootBtn = (Button) view.findViewById(R.id.logOutBttn);
        logootBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutUser();
            }
        });

        //postavljanje vrijednosti
        imageView.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.person_no_image));
        ime_prezime.setText("Korisničko ime: " + sm.getUsername());
        email.setText("Email: " + sm.getEmail());
        donacija.setText("Donacija: " + String.valueOf(sm.getDonacija()) + " kn");

        return view;
    }

    private void logoutUser() {
        sm.setLogin(false);

        //pokretanje login activity-a
        Intent intent = new Intent(getActivity(), Login.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        getActivity().startActivity(intent);
    }
}
