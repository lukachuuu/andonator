package tvz.mc;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tvz.mc.Adapters.ActiveDonationsAdapter;
import tvz.mc.Helper.AppConfig;
import tvz.mc.Model.ActiveDonations;


public class ActiveDonationsFragment extends Fragment implements AdapterView.OnItemClickListener {

    private static final String TAG = ActiveDonations.class.getSimpleName();
    private ListView listView;
    private FragmentManager fm;
    private AlertDialog.Builder builder;
    private ProgressDialog progressDialog;
    public static int pos;
    private ActiveDonationsAdapter adapter;
    int size = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fm = getFragmentManager();

        //inicijalizacija progress dialog-a i postavljanje poruke koja se ispisuje
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Učitavanje...");

        //inicijalizacija buildera
        builder = new AlertDialog.Builder(getActivity());

        //pozivanje metode za dohvacanje podataka
        makeJsonArrayRequest();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //postavljanje view-a
        View view = inflater.inflate(R.layout.active_donations_list_view, container, false);

        //povezivanje s id-om
        listView = (ListView) view.findViewById(R.id.list_view_donations);

        //incijalizacija adaptera
        adapter = new ActiveDonationsAdapter(getActivity());

        //postavljanje adaptera i listenera kojeg klasa implementira
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        return view;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view,int position, long id) {

        for (int i = 0; i < size; i++) {
            //provjera s id-om koji zapravo predstavlja poziciju
            if (position == ActiveDonationsAdapter.getListaDonacija().get(i).getMojId()) {

                //sprema se pozicija
                pos = position;

                //pocetni fragment u containeru, koji ce se kasnije mijenjati pomocu fragment transakcija i tagova
                //dodavanje u back stack
                FragmentTransaction transaction = fm.beginTransaction();
                Fragment quiz = new ActiveDonationsQuizFragment();
                transaction.add(R.id.frag, quiz, "quiz");
                transaction.hide(fm.findFragmentByTag("active"));
                transaction.show(quiz);
                transaction.addToBackStack("quiz");
                transaction.commit();

                //postavljanje naslova action bar-a
                try {
                    getActivity().getActionBar().setTitle(ActiveDonationsAdapter.getListaDonacija().get(i).getNaslov());
                    throw new NullPointerException();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void makeJsonArrayRequest() {

        //prikaz progress dialoga
        showpDialog();

        JsonArrayRequest req = new JsonArrayRequest(AppConfig.URL_ACTIVE,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());

                        //sakrivanje progress dialoga
                        hidepDialog();

                        try {

                            for (int i = 0; i < response.length(); i++) {

                                JSONObject projekt = (JSONObject) response.get(i);

                                //dobivanje vrijednosti iz JSON-a
                                Integer id = projekt.getInt("id");
                                String naslov = projekt.getString("naslov");
                                String opis = projekt.getString("opis");
                                String sopis = projekt.getString("short_opis");
                                String voditelj = projekt.getString("voditelj_kampanje");
                                double trenutnaDonacija = projekt.getDouble("trenutna_donacija");
                                double max_donacija = projekt.getDouble("max_donacija");
                                String slika = projekt.getString("slika");
                                String datum = projekt.getString("datumIzrade");

                                ActiveDonations activeTmp = new ActiveDonations(naslov, sopis, opis, voditelj, trenutnaDonacija,
                                        max_donacija, slika, dateExtract(datum), i, getResources().getDrawable(R.drawable.bg_splash),
                                        "DONIRAJ", ((trenutnaDonacija / max_donacija) * 100));

                                //pomocu definiranog settera postavlja se vrijednost
                                adapter.setListaDonacija(activeTmp);
                                //sluzi za refresh, odnosno javlja da su se dogodile promjene
                                adapter.notifyDataSetChanged();

                                Log.d(TAG, naslov + " " + opis);
                            }

                            size = ActiveDonationsAdapter.getListaDonacija().size();
                            listView.setAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
//                            Toast.makeText(getActivity(),
//                                    "Error: " + e.getMessage(),
//                                    Toast.LENGTH_LONG).show();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Error: " + error.getMessage());
                try {
//                Toast.makeText(getActivity(),
//                        error.getMessage(), Toast.LENGTH_SHORT).show();

                    //postavlja se alert dialog za upozorenje
                    builder.setMessage("Provjerite internet vezu")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            }).create().show();
                    //sakrivanje progress dialoga
                    hidepDialog();
                    //da se aplikacija ne crash-a
                    throw new Exception();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        //dodavanje u red requesta
        Splash.getInstance().addToRequestQueue(req);
    }

    //metoda pomocu koje se izdvajaju dijelovi datuma, i spajaju u potreban format
    public String dateExtract(String datum) {

        String dan = datum.substring(8, 10);
        String mjesec = datum.substring(5, 7);
        String godina = datum.substring(0, 4);

        datum = dan + "." + mjesec + "." + godina + ".";

        return datum;
    }

    //metoda za prikaz progress dialoga
    private void showpDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    //metoda za sakrivanje progress dialoga
    private void hidepDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }
}







