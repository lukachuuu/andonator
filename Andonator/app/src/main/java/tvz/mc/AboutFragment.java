package tvz.mc;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import tvz.mc.Model.About;

public class AboutFragment extends Fragment {

    private ArrayList<About> listaONama;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //inicijalizacija liste i doadavanje vrijednosti
        listaONama = new ArrayList<>();
        listaONama.add(new About("Kako funkcionira aplikacija?", "Aplikacija Andonator uz Vašu pomoć (odgovaranjem na pitanja) donira potrebna sredstva udrugama, zakladama itd.\n" +
                "Aplikacijom Andonator prvenstveno želimo donacijama pomoći onima kojima je to najpotrebnije, s druge strane nudimo novi način oglašavanja. Nismo željeli stvoriti još jedan sustav nagrađivanja vjernosti korisnika, nego smo htjeli da na ovaj jednostavan način pomognete drugima."
        ));
        listaONama.add(new About("Kako pomoći pri doniranju?", "Kako biste pomogli uspjehu donacije potrebno je otvoriti i pročitati jedan od članaka koje možete pronaći na Početnoj kartici. Nakon što ste pročitali promotivni članak kliknite na gumb ‘Riješi kviz i doniraj’. Otvorit će vam se kviz u kojem se nalazi najmanje jedno pitanje. Odaberite odgovore koje smatrate točnima i kliknite ‘Završi’. Ukoliko je Vaš odgovor točan oglašivač, odnosno autor pročitanog članka, će donirati određenu količinu sredstava nekoj udruzi ili zakladi."));
        listaONama.add(new About("Tko su oglašivači?", "Oglašivači su tvrtke i agencije koje nam se jave sa željom doniranja i oglašavanja u našoj aplikaciji. Oglašivači šalju svoje promotivne članke i kvizove koje ćemo mi objaviti u aplikaciji."));
        listaONama.add(new About("Koja se sredstva doniraju?", "Doniraju se ona sredstva koja odabire Oglašivač. To može biti novac, hrana, odjeća, igračke, promotivni proizvodi tvrtke pa čak i fizički rad radnika (npr. uređenje dječjeg igrališta). Aplikacija Andonator osigurava koju minimalnu količinu sredstava Oglašivač mora donirati po jednom točnom odgovoru, a Oglašivač određuje koliko će maksimalno donirati tijekom cijele promocije."));
        listaONama.add(new About("Kome se doniraju sredstva?", "Sva sredstva koja se prikupe tijekom promocije doniraju se unaprijed dogovorenoj udruzi, zakladi i svima onima kojima je to najpotrebnije, a prijave se u naš sustav."));

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        TextView about1, about_answer1,about2, about_answer2,about3, about_answer3,about4, about_answer4,about5, about_answer5;

        //postavljanje view-a
        View view = inflater.inflate(R.layout.about_fragment, container, false);

        //povezivanje s id-om
        about1 = (TextView) view.findViewById(R.id.about1);
        about_answer1 = (TextView) view.findViewById(R.id.about_answer1);

        about2 = (TextView) view.findViewById(R.id.about2);
        about_answer2 = (TextView) view.findViewById(R.id.about_answer2);

        about3 = (TextView) view.findViewById(R.id.about3);
        about_answer3 = (TextView) view.findViewById(R.id.about_answer3);

        about4 = (TextView) view.findViewById(R.id.about4);
        about_answer4 = (TextView) view.findViewById(R.id.about_answer4);

        about5 = (TextView) view.findViewById(R.id.about5);
        about_answer5 = (TextView) view.findViewById(R.id.about_answer5);

        //postavljanje teksta
        about1.setText(listaONama.get(0).getQuestion());
        about_answer1.setText(listaONama.get(0).getAnswer());

        about2.setText(listaONama.get(1).getQuestion());
        about_answer2.setText(listaONama.get(1).getAnswer());

        about3.setText(listaONama.get(2).getQuestion());
        about_answer3.setText(listaONama.get(2).getAnswer());

        about4.setText(listaONama.get(3).getQuestion());
        about_answer4.setText(listaONama.get(3).getAnswer());

        about5.setText(listaONama.get(4).getQuestion());
        about_answer5.setText(listaONama.get(4).getAnswer());

        return view;
    }
}
