package tvz.mc;


import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import tvz.mc.Adapters.ActiveDonationsAdapter;
import tvz.mc.Model.ActiveDonationsQuiz;

public class ActiveDonationsQuizFragment extends Fragment implements View.OnClickListener, FragmentManager.OnBackStackChangedListener {

    private ArrayList<ActiveDonationsQuiz> list;
    private FragmentManager fm;
    int position;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fm = getFragmentManager();
        fm.addOnBackStackChangedListener(this);

        list = new ArrayList<>();
        //dohvat kliknute pozicije
        position = ActiveDonationsFragment.pos;

        //dodavanje vrijednosti u listu prema poziciji
        list.add(new ActiveDonationsQuiz(ActiveDonationsAdapter.getListaDonacija().get(position).getNaslov(),
                ActiveDonationsAdapter.getListaDonacija().get(position).getShort_opis(), ActiveDonationsAdapter.getListaDonacija().get(position).getOpis(),
                ActiveDonationsAdapter.getListaDonacija().get(position).getVoditelj_kampanje(), ActiveDonationsAdapter.getListaDonacija().get(position).getTrenutna_donacija(),
                ActiveDonationsAdapter.getListaDonacija().get(position).getMax_donacija(), ActiveDonationsAdapter.getListaDonacija().get(position).getSlika(),
                ActiveDonationsAdapter.getListaDonacija().get(position).getDatum(), ActiveDonationsAdapter.getListaDonacija().get(position).getMojId(),
                ActiveDonationsAdapter.getListaDonacija().get(position).getDrawable(), ActiveDonationsAdapter.getListaDonacija().get(position).getProgress(), "", "ZARAĐENO:", "Riješi i doniraj", "SHARE"));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        RelativeLayout relativeLayout;
        TextView naslov, datum, oProjektu, share, zaradeno;
        ProgressBar progressBar;
        Button rijesi;

        //postavljanje viewa
        View view = inflater.inflate(R.layout.active_donations_quiz_fragment, container, false);

        //povezivanje s id-om
        relativeLayout = (RelativeLayout) view.findViewById(R.id.background_image_quiz);
        naslov = (TextView) view.findViewById(R.id.project_title_quiz);
        datum = (TextView) view.findViewById(R.id.date_quiz);
        oProjektu = (TextView) view.findViewById(R.id.about_project_quiz);
        share = (TextView) view.findViewById(R.id.tv_share);
        progressBar = (ProgressBar) view.findViewById(R.id.progress_quiz);
        zaradeno = (TextView) view.findViewById(R.id.earned_quiz);
        rijesi = (Button) view.findViewById(R.id.solve_donate_quiz);


        //stvaranje dohvacene slike iz baze za prikaz
        Bitmap tmp = ActiveDonationsAdapter.decodeBase64(list.get(0).getSlika());
        Drawable dr = new BitmapDrawable(tmp);

        //postavljanje vrijeddnosti
        relativeLayout.setBackground(dr);
        naslov.setText(list.get(0).getNaslov());
        datum.setText(list.get(0).getDatum());
        oProjektu.setText(list.get(0).getOpis());
        share.setText(list.get(0).getShare());
        progressBar.setProgress((int) list.get(0).getProgress());
        zaradeno.setText(list.get(0).getZaradeno() + " " + list.get(0).getTrenutna_donacija() + " kn");
        rijesi.setText(list.get(0).getRijesiIDoniraj());

        //postavljanje listenera kojeg klasa implementira
        share.setOnClickListener(this);
        rijesi.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        if (R.id.solve_donate_quiz == v.getId()) {

            //pocetni fragment u containeru, koji ce se kasnije mijenjati pomocu fragment transakcija i tagova
            //dodavanje u back stack
            Fragment questionsFragment = new QuestionsFragment();
            FragmentTransaction transaction = fm.beginTransaction();
            transaction.add(R.id.frag, questionsFragment, "question");
            transaction.hide(fm.findFragmentByTag("quiz"));
            transaction.show(questionsFragment);
            transaction.addToBackStack("question");
            transaction.commit();
        } else if (v.getId() == R.id.tv_share) {
            //za sad samo toast
            Toast.makeText(getActivity(), "share", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackStackChanged() {

    }
}
