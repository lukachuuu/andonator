package tvz.mc;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import tvz.mc.Helper.AppConfig;
import tvz.mc.Helper.SessionManager;

public class Login extends Activity {

    private static final String TAG = Login.class.getSimpleName();
    private Button logBttn;
    private Button regBttn;
    private EditText inputEmail;
    private EditText inputPassword;
    private ProgressDialog pDialog;
    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //full screen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.login_activity);

        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        logBttn = (Button) findViewById(R.id.btnLogin);
        regBttn = (Button) findViewById(R.id.btnLinkToRegisterScreen);

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        session = new SessionManager(getApplicationContext());

        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            Intent intent = new Intent(Login.this,
                    Main.class);
            startActivity(intent);
            finish();
        }


        inputPassword.setTypeface(Typeface.DEFAULT);
        inputPassword.setTransformationMethod(new PasswordTransformationMethod());


        logBttn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = inputEmail.getText().toString();
                String password = inputPassword.getText().toString();

                // Check for empty data in the form
                if (email.trim().length() > 0 && password.trim().length() > 0) {
                    // login user
                    checkLogin(email, password);
                } else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Unesite podatke!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });


        regBttn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this, Registration.class));
            }
        });

    }

    private void checkLogin(final String email, final String password) {
        String tag_string_req = "req_login";

        pDialog.setMessage("Prijava...");
        //prikaz progress dialoga
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST, AppConfig.URL_LOGIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Odgovor: " + response.toString());
                //sakrivanje progress dialoga
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");


                    // Provjera greski
                    if (!error) {

                        // TREBA NAPRAVITI SESIJU
                        session.setLogin(true);

                        JSONArray userDetails = jObj.getJSONArray("user");

                        try {

                            JSONObject oneObject = userDetails.getJSONObject(0);
                            String username = oneObject.getString("ime_prezime");
                            String email = oneObject.getString("email");
                            Integer id = oneObject.getInt("id");
                            String apiKey = oneObject.getString("apiKey");
                            double donacija = oneObject.getDouble("donacija");

                            Log.d(TAG, "žžžždonacija je:" + donacija);
                            session.setUserDetails(username, email, id, apiKey, (float) donacija);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // Launch main activity
                        Intent intent = new Intent(Login.this, Main.class);
                        startActivity(intent);
                        finish();

                    } else {
                        //prikaz greski
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login greska: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Greška\nProvjerite internet vezu", Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // stavljanje elemenata u POST
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("password", password);

                return params;
            }

        };

        //dodavanje u red requesta
        Splash.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    //metoda za prikaz progress dialoga
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    //metoda za sakrivanje progress dialoga
    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}