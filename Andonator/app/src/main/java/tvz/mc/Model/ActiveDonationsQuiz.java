package tvz.mc.Model;


import android.graphics.drawable.Drawable;

public class ActiveDonationsQuiz extends ActiveDonations {

    private String zaradeno, rijesiIDoniraj, share;

    public ActiveDonationsQuiz(String naslov, String short_opis, String opis, String voditelj_kampanje, double trenutna_donacija, double max_donacija, String slika, String datum, int mojId, Drawable drawable, double progress, String donacija, String zaradeno, String rijesiIDoniraj, String share) {
        super(naslov, short_opis, opis, voditelj_kampanje, trenutna_donacija, max_donacija, slika, datum, mojId, drawable, donacija, progress);
        this.zaradeno = zaradeno;
        this.rijesiIDoniraj = rijesiIDoniraj;
        this.share = share;
    }

    public String getRijesiIDoniraj() {
        return rijesiIDoniraj;
    }

    public String getZaradeno() {
        return zaradeno;
    }

    public String getShare() {
        return share;
    }
}
