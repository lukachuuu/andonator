package tvz.mc.Model;


import android.graphics.drawable.Drawable;

public class ActiveDonations {
    private String naslov, short_opis, opis, voditelj_kampanje, slika, datum, donacija;
    private double trenutna_donacija, max_donacija;
    private double progress;
    private int mojId;
    private Drawable drawable;


    public String getNaslov() {
        return naslov;
    }


    public String getShort_opis() {
        return short_opis;
    }

    public String getOpis() {
        return opis;
    }

    public String getVoditelj_kampanje() {
        return voditelj_kampanje;
    }

    public double getTrenutna_donacija() {
        return trenutna_donacija;
    }

    public double getMax_donacija() {
        return max_donacija;
    }

    public String getSlika() {
        return slika;
    }

    public String getDatum() {
        return datum;
    }

    public int getMojId() {
        return mojId;
    }

    public void setId(int id) {
        this.mojId = id;
    }

    public String getDonacija() {
        return donacija;
    }

    public ActiveDonations(String naslov, String short_opis, String opis, String voditelj_kampanje, double trenutna_donacija, double max_donacija, String slika, String datum, int mojId, Drawable drawable, String donacija, double progress) {
        this.naslov = naslov;
        this.short_opis = short_opis;
        this.opis = opis;
        this.voditelj_kampanje = voditelj_kampanje;
        this.trenutna_donacija = trenutna_donacija;
        this.max_donacija = max_donacija;
        this.slika = slika;
        this.datum = datum;
        this.mojId = mojId;
        this.drawable = drawable;
        this.donacija = donacija;
        this.progress = progress;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public double getProgress() {
        return progress;
    }
}
