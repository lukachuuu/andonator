package tvz.mc.Model;


public class About {
    private String question, answer;

    public About(String howItWorks, String howItWorksAnswer) {
        this.question = howItWorks;
        this.answer = howItWorksAnswer;
    }

    public String getQuestion() {
        return question;
    }

    public String getAnswer() {
        return answer;
    }

}
