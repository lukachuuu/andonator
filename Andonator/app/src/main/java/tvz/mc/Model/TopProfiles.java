package tvz.mc.Model;

import android.graphics.drawable.Drawable;

public class TopProfiles {
    private String username;
    private double donations;
    private Drawable icon;

    public TopProfiles(String username, double donations, Drawable icon) {
        this.username = username;
        this.donations = donations;
        this.icon = icon;
    }

    public String getUsername() {
        return username;
    }

    public double getDonations() {
        return donations;
    }

    public Drawable getIcon() {
        return icon;
    }
}
