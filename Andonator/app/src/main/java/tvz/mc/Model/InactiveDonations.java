package tvz.mc.Model;

import android.graphics.drawable.Drawable;

public class InactiveDonations extends ActiveDonations {
    public InactiveDonations(String naslov, String short_opis, String opis, String voditelj_kampanje, Integer trenutna_donacija, Integer max_donacija, String slika, String datum, int mojId, Drawable drawable, String donacija, int progress) {
        super(naslov, short_opis, opis, voditelj_kampanje, trenutna_donacija, max_donacija, slika, datum, mojId, drawable, donacija, 0);
    }
}
