package tvz.mc;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

import tvz.mc.Helper.AppConfig;


public class QuestionsFragment extends Fragment {

    private static final String TAG = QuestionsFragment.class.getSimpleName();

    private Button submitBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        makeJsonObjectRequest();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //postavljanje view-a
        View view = inflater.inflate(R.layout.questions_fragment, container, false);


        submitBtn = (Button) view.findViewById(R.id.submit);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                Toast.makeText(getActivity(), "Hvala!", Toast.LENGTH_SHORT).show();

                //vracanje natrag na aktivne donacije
                if (fm.findFragmentByTag("quiz").isHidden()
                        && fm.findFragmentByTag("active").isHidden()
                        && fm.findFragmentByTag("question").isVisible()) {

                    Fragment f = fm.findFragmentByTag("active");
                    fm.beginTransaction()
                            .hide(fm.findFragmentByTag("question"))
                            .show(f).commit();

                    try {
                        getActivity().getActionBar().setTitle("Aktivne donacije");
                        throw new NullPointerException();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        return view;
    }

    private void makeJsonObjectRequest() {


        JsonArrayRequest jsonObjReq = new JsonArrayRequest(AppConfig.TEST + "2", new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
        //dohvacanje podataka o kratkom testu

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(getActivity(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        Splash.getInstance().addToRequestQueue(jsonObjReq);
    }

}