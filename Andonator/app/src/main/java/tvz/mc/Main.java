package tvz.mc;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;

import tvz.mc.Adapters.SampleFragmentPagerAdapter;
import tvz.mc.Helper.SessionManager;
import tvz.mc.Helper.SlidingTabLayout;


public class Main extends FragmentActivity {

    private SessionManager session;
    private ViewPager viewPager;
    FragmentManager fm;
    int index;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        session = new SessionManager(getApplicationContext());

        fm = getSupportFragmentManager();

        Log.d("TEST SESSION", session.getUsername());

        //potrebna prijava ako korisnik nije prijavljen
        if (!session.isLoggedIn()) {
            Intent intent = new Intent(Main.this, Login.class);
            startActivity(intent);
            finish();
        }

        try {
            //default naslov
            getActionBar().setTitle("Aktivne donacije");
            throw new NullPointerException();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }


        //povezivanje s id-om
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        //postavljanje adaptera
        viewPager.setAdapter(new SampleFragmentPagerAdapter(getSupportFragmentManager(),
                Main.this));
        viewPager.setOffscreenPageLimit(4);


        final SlidingTabLayout slidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        // dodavanje custom taba prikazu
        slidingTabLayout.setCustomTabView(R.layout.custom_tab, 0);
        // centriranje tabova
        slidingTabLayout.setDistributeEvenly(true);
        // postavljanje boje
        slidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return Color.WHITE;
            }
        });
        //dodavanje view pagera sliding tab layoutu
        slidingTabLayout.setViewPager(viewPager);
        slidingTabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            //promjena naslova s promjenom pozicije
            @Override
            public void onPageSelected(int position) {

                if (position == 0) {
                    getActionBar().setTitle("Aktivne donacije");
                    index = position;
                } else if (position == 1) {
                    getActionBar().setTitle("Arhiva donacija");
                    index = position;
                } else if (position == 2) {
                    getActionBar().setTitle("Top profili");
                    index = position;
                } else if (position == 3) {
                    getActionBar().setTitle("Korisnički račun");
                    index = position;
                } else if (position == 4) {
                    getActionBar().setTitle("O nama");
                    index = position;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onBackPressed() {

        //prije izlaska iz aplikacije vrati se na pocetni tab
        if (index != 0) {
            viewPager.setCurrentItem(0);
        }
        //ako su prikazani aktivni projekti, izlaz iz aplikacije
        else if (fm.findFragmentByTag("active").isVisible()) {
            finish();
        }
        //vracanje na aktivne projekte
        else if (fm.findFragmentByTag("active").isHidden()
                && fm.findFragmentByTag("quiz").isVisible()
                && viewPager.getCurrentItem() == 0) {

            Fragment f = fm.findFragmentByTag("active");
            fm.beginTransaction()
                    .hide(fm.findFragmentByTag("quiz"))
                    .show(f)
                    .commit();

            try {
                getActionBar().setTitle("Aktivne donacije");
                throw new NullPointerException();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
        //vracanje s kviza, na detalje o projektu
        else if (fm.findFragmentByTag("quiz").isHidden()
                && fm.findFragmentByTag("active").isHidden()
                && fm.findFragmentByTag("question").isVisible()
                && viewPager.getCurrentItem() == 0) {

            Fragment f = fm.findFragmentByTag("quiz");
            fm.beginTransaction()
                    .hide(fm.findFragmentByTag("question"))
                    .show(f).commit();
        } else {
            finish();
            super.onBackPressed();
        }
    }
}
