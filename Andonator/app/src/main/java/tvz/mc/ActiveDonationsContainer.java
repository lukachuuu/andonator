package tvz.mc;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class ActiveDonationsContainer extends Fragment implements FragmentManager.OnBackStackChangedListener {

    private FragmentManager fm;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.active_donations_container, container, false);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //inicijalizacija fragment managera i dodavanje listenera koji klasa implementira
        fm = getFragmentManager();
        fm.addOnBackStackChangedListener(this);

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //pocetni fragment u containeru, koji ce se kasnije mijenjati pomocu fragment transakcija i tagova
        //dodavanje u back stack
        Fragment fragment = new ActiveDonationsFragment();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.add(R.id.frag, fragment, "active");
        transaction.show(fragment);
        transaction.addToBackStack("active");
        transaction.commit();
    }


    @Override
    public void onBackStackChanged() {
        if (fm.getBackStackEntryCount() == 0) getActivity().finish();
    }
}