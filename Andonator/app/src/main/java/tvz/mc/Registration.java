package tvz.mc;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import tvz.mc.Helper.AppConfig;

public class Registration extends Activity {

    private Button registrationBttn;
    private static final String TAG = Registration.class.getSimpleName();
    private Button btnRegister;
    private Button btnLinkToLogin;
    private EditText inputFullName;
    private EditText inputEmail;
    private EditText inputPassword;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.registration_activity);

        //povezivanje s id-om
        inputFullName = (EditText) findViewById(R.id.name);
        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        btnRegister = (Button) findViewById(R.id.btnReg);
        btnLinkToLogin = (Button) findViewById(R.id.btnLinkToLoginScreen);

        inputPassword.setTypeface(Typeface.DEFAULT);
        inputPassword.setTransformationMethod(new PasswordTransformationMethod());

        //progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);


        //natrag na login
        btnLinkToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Registration.this, Login.class);
                startActivity(intent);
            }
        });

        //register button listener
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = inputFullName.getText().toString();
                String email = inputEmail.getText().toString();
                String password = inputPassword.getText().toString();

                //provjera unesenih podataka
                if (!name.isEmpty() && !email.isEmpty() && !password.isEmpty()) {
                    registerUser(name, email, password);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Molimo unesite podatke!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });


    }

    private void registerUser(final String name, final String email, final String password) {
        String tag_string_req = "req_register";

        pDialog.setMessage("Registracija...");
        //prikaz progress dialoga
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST, AppConfig.URL_REGISTER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Register Response: " + response.toString());
                //sakrivanje progress dialoga
                hideDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        // korisnik spremljen u bazu


                        // pokretanje login aktivity-a
                        Intent intent = new Intent(Registration.this, Login.class);
                        startActivity(intent);
                        finish();
                    } else {

                        //ispis greske
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Registration Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // parametri idu u url
                Map<String, String> params = new HashMap<>();
                params.put("ime_prezime", name);
                params.put("email", email);
                params.put("password", password);

                return params;
            }

        };

        //dodavanje u red requesta
        Splash.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    //metoda za prikaz progress dialoga
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    //metoda za sakrivanje progress dialoga
    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}