package tvz.mc.Helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;


public class SessionManager {
    private static String TAG = SessionManager.class.getSimpleName();

    //Shared Preferences
    SharedPreferences pref;

    SharedPreferences.Editor editor;
    Context _context;

    //Shared Preferences mod
    int PRIVATE_MODE = 0;

    //Shared preferences ime datoteke
    private static final String PREF_NAME = "AndonatorLogin";

    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setLogin(boolean isLoggedIn) {

        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);

        //potvrdivanje promjene
        editor.commit();

        Log.d(TAG, "User login session modified!");
    }

    //podaci za sesiju
    public void setUserDetails(String username, String email, Integer id, String apiKey, float donacija) {
        editor.putString("USERNAME", username);
        editor.putString("EMAIL", email);
        editor.putInt("ID", id);
        editor.putString("APIKEY", apiKey);
        editor.putFloat("DONACIJA", donacija);

        editor.commit();
        Log.d(TAG, "Dodani detalji korisnika");
    }

    public String getUsername() {
        return pref.getString("USERNAME", "GRESKA");
    }

    public String getEmail() {
        return pref.getString("EMAIL", "GRESKA");
    }

    public Integer getId() {
        return pref.getInt("ID", 0);
    }

    public String getApiKey() {
        return pref.getString("APIKEY", "GRESKA");
    }

    public float getDonacija() {
        return pref.getFloat("DONACIJA", 0);
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }
}

