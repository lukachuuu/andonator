package tvz.mc.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import tvz.mc.Model.TopProfiles;
import tvz.mc.R;

public class TopProfilesAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;
    private static ArrayList<TopProfiles> listaProfila;
    private TextView username, donations;
    private ImageView avatar;

    public TopProfilesAdapter(Context context) {
        this.context = context;
        listaProfila = new ArrayList<>();
    }


    public static ArrayList<TopProfiles> getListaProfila() {
        return listaProfila;
    }

    public void setListaProfila(TopProfiles profil) {
        listaProfila.add(profil);
    }

    @Override
    public int getCount() {
        return listaProfila.size();
    }

    @Override
    public Object getItem(int position) {
        return listaProfila.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.top_profiles_fragment, parent, false);
        }

        //povezivanje s id-om
        username = (TextView) view.findViewById(R.id.tv_username);
        donations = (TextView) view.findViewById(R.id.tv_donations_num);
        avatar = (ImageView) view.findViewById(R.id.avatar);

        //postavljanje vrijednosti
        username.setText(listaProfila.get(position).getUsername());
        donations.setText(String.valueOf(listaProfila.get(position).getDonations()) + " kn");
        avatar.setBackground(listaProfila.get(position).getIcon());

        return view;
    }
}
