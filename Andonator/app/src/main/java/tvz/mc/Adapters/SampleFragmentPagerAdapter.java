package tvz.mc.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;

import tvz.mc.AboutFragment;
import tvz.mc.ActiveDonationsContainer;
import tvz.mc.InactiveDonationsFragment;
import tvz.mc.R;
import tvz.mc.TopProfilesFragment;
import tvz.mc.UserInfoFragment;


public class SampleFragmentPagerAdapter extends FragmentStatePagerAdapter {
    //broj stranica
    final int PAGE_COUNT = 5;

    private Context context;

    //lista ikona za naslove stranica
    private int[] imageResId = {
            R.drawable.active,
            R.drawable.archive,
            R.drawable.top,
            R.drawable.user,
            R.drawable.about

    };

    public SampleFragmentPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }


    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {

        //vraca se potrebni fragment ovisno o poziciji
        switch (position) {
            case 0:
                return new ActiveDonationsContainer();
            case 1:
                return new InactiveDonationsFragment();
            case 2:
                return new TopProfilesFragment();
            case 3:
                return new UserInfoFragment();
            case 4:
                return new AboutFragment();
            default:
                break;
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        //generira se naslov na temelju pozicije
        Drawable image = context.getResources().getDrawable(imageResId[position]);
        image.setBounds(0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight());
        SpannableString sb = new SpannableString(" ");
        ImageSpan imageSpan = new ImageSpan(image, ImageSpan.ALIGN_BOTTOM);
        sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        notifyDataSetChanged();
        return sb;
    }
}