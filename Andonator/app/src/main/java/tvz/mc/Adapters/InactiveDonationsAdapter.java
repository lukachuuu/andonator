package tvz.mc.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import tvz.mc.Model.InactiveDonations;
import tvz.mc.R;


public class InactiveDonationsAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;
    private static ArrayList<InactiveDonations> listaDonacija;
    private RelativeLayout background_image;
    private TextView naslov;
    private TextView datum;
    private TextView shortOpis;


    public InactiveDonationsAdapter(Context context) {
        this.context = context;
        listaDonacija = new ArrayList<>();
    }

    public static ArrayList<InactiveDonations> getListaDonacija() {
        return listaDonacija;
    }

    public void setListaDonacija(InactiveDonations donacija) {
        listaDonacija.add(donacija);
    }

    @Override
    public int getCount() {
        return listaDonacija.size();
    }

    @Override
    public Object getItem(int position) {
        return listaDonacija.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.inactive_donations_fragment, parent, false);
        }

        //povezivanje s id-om
        background_image = (RelativeLayout) view.findViewById(R.id.background_image_inactive);
        naslov = (TextView) view.findViewById(R.id.project_title_inactive);
        datum = (TextView) view.findViewById(R.id.date_inactive);
        shortOpis = (TextView) view.findViewById(R.id.about_project_inactive);

        //stvaranje dohvacene slike iz baze za prikaz
        Bitmap tmp = decodeBase64(listaDonacija.get(position).getSlika());
        Drawable dr = new BitmapDrawable(tmp);

        //postavljanje vrijeddnosti
        background_image.setBackground(dr);
        naslov.setText(listaDonacija.get(position).getNaslov());
        datum.setText(listaDonacija.get(position).getDatum());
        shortOpis.setText(listaDonacija.get(position).getShort_opis());

        return view;
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }
}
