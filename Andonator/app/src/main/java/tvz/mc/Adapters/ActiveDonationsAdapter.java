package tvz.mc.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import tvz.mc.Model.ActiveDonations;
import tvz.mc.R;


public class ActiveDonationsAdapter extends BaseAdapter {

    private Context context;

    private static ArrayList<ActiveDonations> listaDonacija;


    public ActiveDonationsAdapter(Context context) {
        this.context = context;
        listaDonacija = new ArrayList<>();
    }

    public static ArrayList<ActiveDonations> getListaDonacija() {
        return listaDonacija;
    }

    public void setListaDonacija(ActiveDonations donacija) {
        listaDonacija.add(donacija);
    }

    @Override
    public int getCount() {
        return listaDonacija.size();
    }

    @Override
    public Object getItem(int position) {
        return listaDonacija.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater;
        RelativeLayout background_image;
        TextView naslov, datum, shortOpis, donate;
        ProgressBar progressBar;


        View view = convertView;
        if (view == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.active_donations_fragment, parent, false);
        }

        //povezivanje s id-om
        background_image = (RelativeLayout) view.findViewById(R.id.background_image);
        naslov = (TextView) view.findViewById(R.id.project_title);
        datum = (TextView) view.findViewById(R.id.date);
        shortOpis = (TextView) view.findViewById(R.id.about_project);
        donate = (TextView) view.findViewById(R.id.tv_donate);
        progressBar = (ProgressBar) view.findViewById(R.id.progress);

        //stvaranje dohvacene slike iz baze za prikaz pomocu stvorene metode
        Bitmap tmp = decodeBase64(listaDonacija.get(position).getSlika());
        Drawable dr = new BitmapDrawable(tmp);

        //postavljanje vrijeddnosti
        background_image.setBackground(dr);
        naslov.setText(listaDonacija.get(position).getNaslov());
        datum.setText(listaDonacija.get(position).getDatum());
        shortOpis.setText(listaDonacija.get(position).getShort_opis());
        donate.setText(listaDonacija.get(position).getDonacija());
        progressBar.setProgress((int) listaDonacija.get(position).getProgress());

        return view;
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }
}
