package tvz.mc;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tvz.mc.Adapters.InactiveDonationsAdapter;
import tvz.mc.Helper.AppConfig;
import tvz.mc.Model.InactiveDonations;


public class InactiveDonationsFragment extends Fragment {

    private static final String TAG = InactiveDonations.class.getSimpleName();
    private ListView listView;
    private InactiveDonationsAdapter adapter;
    int size = 0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //pozivanje metodea za dohvacanje podataka
        makeJsonArrayRequest();

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //postavljanje view-a
        View view = inflater.inflate(R.layout.inactive_donations_list_view, container, false);

        //povezivanje s id-om
        listView = (ListView) view.findViewById(R.id.list_view_donations_inactive);

        //incijalizacija adaptera
        adapter = new InactiveDonationsAdapter(getActivity());

        //postavljanje adaptera
        listView.setAdapter(adapter);

        return view;
    }


    private void makeJsonArrayRequest() {

        JsonArrayRequest req = new JsonArrayRequest(AppConfig.URL_INACTIVE,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());

                        try {
                            for (int i = 0; i < response.length(); i++) {

                                JSONObject projekt = (JSONObject) response.get(i);

                                //dobivanje vrijednosti iz JSON-a
                                Integer id = projekt.getInt("id");
                                String naslov = projekt.getString("naslov");
                                String opis = projekt.getString("opis");
                                String sopis = projekt.getString("short_opis");
                                String voditelj = projekt.getString("voditelj_kampanje");
                                Integer trenutnaDonacija = projekt.getInt("trenutna_donacija");
                                Integer max_donacija = projekt.getInt("max_donacija");
                                String slika = projekt.getString("slika");
                                String datum = projekt.getString("datumIzrade");

                                InactiveDonations activeTmp = new InactiveDonations(naslov, sopis, opis, voditelj, trenutnaDonacija, max_donacija, slika, dateExtract(datum), i, getResources().getDrawable(R.drawable.bg_splash), "", 0);

                                //pomocu definiranog settera postavlja se vrijednost
                                adapter.setListaDonacija(activeTmp);
                                //sluzi za refresh, odnosno javlja da su se dogodile promjene
                                adapter.notifyDataSetChanged();

                                Log.d(TAG, naslov + " " + opis);
                            }

                            size = InactiveDonationsAdapter.getListaDonacija().size();
                            listView.setAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
//                            Toast.makeText(getActivity(),
//                                    "Error: " + e.getMessage(),
//                                    Toast.LENGTH_LONG).show();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Error: " + error.getMessage());
//                Toast.makeText(getActivity(),
//                        error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        //dodavanje u red requesta
        Splash.getInstance().addToRequestQueue(req);
    }

    //metoda pomocu koje se izdvajaju dijelovi datuma, i spajaju u potreban format
    public String dateExtract(String datum) {

        String dan = datum.substring(8, 10);
        String mjesec = datum.substring(5, 7);
        String godina = datum.substring(0, 4);

        datum = dan + "." + mjesec + "." + godina + ".";

        return datum;
    }
}







