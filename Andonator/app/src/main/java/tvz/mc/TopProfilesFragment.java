package tvz.mc;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tvz.mc.Adapters.TopProfilesAdapter;
import tvz.mc.Helper.AppConfig;
import tvz.mc.Model.TopProfiles;

public class TopProfilesFragment extends Fragment {

    private static final String TAG = TopProfiles.class.getSimpleName();

    private ListView listView;
    private TopProfilesAdapter adapter;
    private int size = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        makeJsonArrayRequest();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.top_profiles_list_view, container, false);
        listView = (ListView) view.findViewById(R.id.list_view_top_profiles);

        adapter = new TopProfilesAdapter(getActivity());
        listView.setAdapter(adapter);

        return view;
    }

    private void makeJsonArrayRequest() {

        JsonArrayRequest req = new JsonArrayRequest(AppConfig.TOP_USERS,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());

                        try {

                            for (int i = 0; i < response.length(); i++) {

                                JSONObject projekt = (JSONObject) response.get(i);

                                //dobivanje vrijednosti iz JSON-a
                                Integer id = projekt.getInt("id");
                                String ime_prezime = projekt.getString("ime_prezime");
                                String email = projekt.getString("email");
                                double donacija = projekt.getDouble("donacija");


                                TopProfiles activeTmp = new TopProfiles(i + 1 + "." + ime_prezime, donacija, getResources().getDrawable(R.drawable.person_no_image));
                                adapter.setListaProfila(activeTmp);
                                adapter.notifyDataSetChanged();

                                Log.d(TAG, id + " " + email);
                            }

                            //pomocu definiranog settera postavlja se vrijednost
                            size = TopProfilesAdapter.getListaProfila().size();
                            //sluzi za refresh, odnosno javlja da su se dogodile promjene
                            listView.setAdapter(adapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
//                            Toast.makeText(getActivity(),
//                                    "Error: " + e.getMessage(),
//                                    Toast.LENGTH_LONG).show();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Error: " + error.getMessage());
//                Toast.makeText(getActivity(),
//                        error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

        //dodavanje u red requesta
        Splash.getInstance().addToRequestQueue(req);
    }
}
