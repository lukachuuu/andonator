package tvz.mc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;


public class Splash extends Activity {

    public static final String TAG = Splash.class.getSimpleName();

    private RequestQueue mRequestQueue;

    private static Splash mInstance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);

        mInstance = this;

        Thread timer = new Thread() {
            @Override
            public void run() {
                try {
                    //trajanje splash-a (1.5 sec)
                    sleep(1500);
                    startActivity(new Intent(Splash.this, Login.class));
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    finish();
                }
            }
        };
        //pokretanje splash-a
        timer.start();
    }

    public static synchronized Splash getInstance() {
        return mInstance;
    }

    //vracanje request
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    //dadavanje requesta
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}